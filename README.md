# LendTech Backend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
```
The project is served by the backend application running on localhost port 8080. Make sure the backend application runs first before running it. You can change the path as desired.
```

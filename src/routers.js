import HomePage from "@/components/HomePage/HomePage"
import SignUp from "@/components/SignUp/SignUp"
import LoginPage from "@/components/LoginPage/LoginPage"
import PaymentsPage from "@/components/PaymentsPage/PaymentsPage"
import LoansPage from "@/components/LoansPage/LoansPage"
import RecordLoanPage from "@/components/RecordLoanPage/RecordLoanPage";
import {createRouter, createWebHistory} from 'vue-router'

const routes = [
    {
        name: 'HomePage',
        component: HomePage,
        path:'/'
    },
    {
        name: 'SignUp',
        component: SignUp,
        path:'/sign-up'
    },
    {
        name: 'LoginPage',
        component: LoginPage,
        path:'/login'
    },
    {
        name: 'PaymentsPage',
        component: PaymentsPage,
        path:'/payments'
    },
    {
        name: 'LoansPage',
        component: LoansPage,
        path:'/loans'
    },
    {
        name: 'RecordLoanPage',
        component: RecordLoanPage,
        path:'/loans/record'
    }
];

const router = createRouter({
    history:createWebHistory(),
    routes
})

export default router
